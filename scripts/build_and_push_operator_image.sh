#!/bin/bash

set -eo pipefail

source scripts/_common.sh
source scripts/create_release_config.sh

if "$CERTIFIED"; then
  PROJECT="$GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE"
else
  PROJECT="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE"
fi

echo "Building operator image $PROJECT/$GITLAB_RUNNER_OPERATOR_IMAGE:$OPERATOR_VERSION"

# REVISION is already included in the $OPERATOR_VERSION that was passed down from the initial make target
make PROJECT="$PROJECT" VERSION="$OPERATOR_VERSION" REVISION="" \
  docker-build docker-tag
login_and_push operator "$CERTIFIED"