/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"os"
	"reflect"
	"time"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	runnerctl "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/runner"
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	cleanupFinalizer string = "finalizer.gitlab.com"

	// ErrTokenNotFound returns when token secret is not found
	ErrTokenNotFound runnerctl.CustomError = "token secret not found"
)

// RunnerReconciler reconciles a Runner object
type RunnerReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners/finalizers,verbs=update;patch;delete
// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=serviceaccounts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=rolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/log,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/exec,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/attach,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=resourcequotas,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/proxy,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/status,verbs=get;list;watch;create;update;patch;delete

// Reconcile triggers when an event occurs on the watched resource
func (r *RunnerReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("runner", req.NamespacedName)

	log.Info("Reconciling Runner", "name", req.NamespacedName.Name, "namespace", req.NamespacedName.Namespace)
	runner := &gitlabv1beta2.Runner{}
	if err := r.Get(ctx, req.NamespacedName, runner); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}

		return ctrl.Result{}, err
	}

	// setup finalizers for garbage collection
	if err := r.setupRunnerFinalizer(ctx, runner); err != nil {
		return ctrl.Result{}, err
	}

	if runner.DeletionTimestamp != nil {
		return ctrl.Result{}, nil
	}

	if err := r.validateRegistrationTokenSecret(ctx, runner); err != nil {
		if err == ErrTokenNotFound {
			return ctrl.Result{RequeueAfter: time.Second * 10}, nil
		}
		return ctrl.Result{}, err
	}

	if err := r.reconcileRBAC(ctx, runner); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.reconcileConfigMaps(ctx, runner); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.reconcileDeployments(ctx, runner, log); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.reconcileStatus(ctx, runner); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager configures the custom resource watched resources
func (r *RunnerReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitlabv1beta2.Runner{}).
		Owns(&corev1.Secret{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&appsv1.Deployment{}).
		// Owns(&monitoringv1.ServiceMonitor{}).
		WithEventFilter(runnerEventsFilter()).
		Complete(r)
}

func runnerEventsFilter() predicate.Predicate {
	return predicate.Funcs{
		DeleteFunc: func(event.DeleteEvent) bool {
			return true
		},
	}
}

func (r *RunnerReconciler) setupRunnerFinalizer(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	origFinalizers := cr.Finalizers

	if cr.DeletionTimestamp == nil && len(cr.Finalizers) == 0 {
		cr.Finalizers = []string{
			cleanupFinalizer,
		}
	}

	if cr.DeletionTimestamp != nil && len(cr.Finalizers) == 1 {
		// clean up shared resources if there are no other
		// GitLab Runner instances in the project
		if err := r.projectCleanup(ctx, cr); err != nil {
			if errors.IsNotFound(err) {
				return nil
			}
			return err
		}

		cr.Finalizers = []string{}
	}

	if !reflect.DeepEqual(origFinalizers, cr.Finalizers) {
		if err := r.Update(ctx, cr); err != nil {
			if errors.IsNotFound(err) {
				return nil
			}
			return err
		}
	}

	return nil
}

func (r *RunnerReconciler) reconcileConfigMaps(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	configs := runnerctl.ConfigMap(cr)

	// check user provided config.toml
	if cr.Spec.Configuration != "" {
		if err := r.getUserConfigToml(ctx, cr); err != nil {
			return err
		}
	}

	found := &corev1.ConfigMap{}
	err := r.Get(ctx, types.NamespacedName{Name: configs.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if errors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, configs, r.Scheme); err != nil {
				return err
			}

			return r.Create(ctx, configs)
		}

		return err
	}

	if cm, changed := gitlabutils.IsConfigMapChanged(found, configs); changed {
		return r.Update(ctx, cm)
	}

	return nil
}

// The getUserConfigToml retrieves the user provided config.tom and merges it with the
// GitLab Runner generated configuration as a configuration template
// https://docs.gitlab.com/runner/register/#runners-configuration-template-file
func (r *RunnerReconciler) getUserConfigToml(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	userCM := &corev1.ConfigMap{}
	userCMKey := types.NamespacedName{
		Name:      cr.Spec.Configuration,
		Namespace: cr.Namespace,
	}

	if err := r.Get(ctx, userCMKey, userCM); err != nil {
		return err
	}

	if _, ok := userCM.Data["config.toml"]; !ok {
		return fmt.Errorf("config.toml not found")
	}

	return nil
}

func (r *RunnerReconciler) reconcileDeployments(ctx context.Context, cr *gitlabv1beta2.Runner, log logr.Logger) error {
	envs := r.parseCustomEnvironments(ctx, cr)
	ctl := runnerctl.New(cr)
	runner := ctl.Deployment(envs)

	if err := r.appendConfigMapChecksum(ctx, runner); err != nil {
		log.Error(err, "Error appending configmap checksums")
	}

	found := &appsv1.Deployment{}
	err := r.Get(ctx, types.NamespacedName{Name: runner.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if errors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, runner, r.Scheme); err != nil {
				return err
			}

			return r.Create(ctx, runner)
		}

		return err
	}

	deployment, changed := gitlabutils.IsDeploymentChanged(found, runner)
	if changed {
		return r.Update(ctx, deployment)
	}

	return nil
}

func (r *RunnerReconciler) reconcileMetrics(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	svc := runnerctl.MetricsService(cr)

	found := &corev1.Service{}
	err := r.Get(ctx, types.NamespacedName{Name: svc.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if errors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, svc, r.Scheme); err != nil {
				return err
			}

			return r.Create(ctx, svc)
		}

		return err
	}

	if !reflect.DeepEqual(svc.Spec, found.Spec) {
		// besides ClusterIP, not much is expected to change
		// return r.Update(ctx, found)
		return nil
	}

	return nil
}

// func (r *RunnerReconciler) reconcileServiceMonitor(ctx context.Context, cr *gitlabv1beta2.Runner) error {

// 	if gitlabutils.IsPrometheusSupported() {
// 		sm := runnerctl.ServiceMonitorService(cr)

// 		found := &monitoringv1.ServiceMonitor{}
// 		err := r.Get(ctx, types.NamespacedName{Name: sm.Name, Namespace: cr.Namespace}, found)
// 		if err != nil {
// 			if errors.IsNotFound(err) {
// 				if err := controllerutil.SetControllerReference(cr, sm, r.Scheme); err != nil {
// 					return err
// 				}

// 				return r.Create(ctx, sm)
// 			}

// 			return err
// 		}

// 		if !reflect.DeepEqual(sm.Spec, found.Spec) {
// 			found.Spec = sm.Spec
// 			return r.Update(ctx, found)
// 		}
// 	}

// 	return nil
// }

func (r *RunnerReconciler) validateRegistrationTokenSecret(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	log := r.Log.WithValues("runner", cr)

	// check if the token secret exists
	tokenSecret := &corev1.Secret{}
	tokenKey := runnerctl.RegistrationTokenSecret(cr)
	if err := r.Get(ctx, tokenKey, tokenSecret); err != nil {
		if errors.IsNotFound(err) {
			log.Error(err, "token secret not found", "name", tokenKey.Name)

			if cr.Status.Phase != RunnerWaiting {
				cr.Status.Phase = RunnerWaiting
				if err := r.updateRunnerStatus(ctx, cr); err != nil {
					return err
				}
			}

			return ErrTokenNotFound
		}

		return nil
	}

	registrationToken, ok := tokenSecret.Data["runner-registration-token"]
	if !ok {
		return fmt.Errorf("runner-registration-token key not found in %s secret", tokenSecret.Name)
	}

	tokenStr := string(registrationToken)
	if tokenStr == "" {
		return fmt.Errorf("runner-registration-token can not be empty")
	}

	if _, ok := tokenSecret.StringData["runner-token"]; !ok {
		tokenSecret.Data["runner-token"] = []byte("")
		return r.Update(ctx, tokenSecret)
	}

	return nil
}

func (r *RunnerReconciler) appendConfigMapChecksum(ctx context.Context, deployment *appsv1.Deployment) error {
	configmaps := gitlabutils.DeploymentConfigMaps(deployment)

	for _, cmName := range configmaps {
		found := &corev1.ConfigMap{}
		err := r.Get(ctx, types.NamespacedName{Name: cmName, Namespace: deployment.Namespace}, found)
		if err != nil {
			return err
		}

		// get checksum from the configmap annotation
		if checksum, ok := found.Annotations["checksum"]; ok {
			// compare the checksum with cm checksum in deployment template annotation
			if val, ok := deployment.Spec.Template.Annotations[cmName]; ok {
				if val != checksum {
					deployment.Spec.Template.Annotations[cmName] = checksum
				}
			} else {
				// account for nil map exception
				if deployment.Spec.Template.Annotations != nil {
					deployment.Spec.Template.Annotations[cmName] = checksum
				} else {
					deployment.Spec.Template.Annotations = map[string]string{
						cmName: checksum,
					}
				}
			}
		}
	}

	return nil
}

func getOperatorNamespace() string { return os.Getenv("OPERATOR_NAMESPACE") }

func (r *RunnerReconciler) reconcileRBAC(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	if err := r.reconcileServiceAccount(ctx, cr); err != nil {
		return err
	}

	if err := r.reconcileRunnerRole(ctx, cr); err != nil {
		return err
	}

	return r.reconcileRunnerRoleBinding(ctx, cr)
}

func (r *RunnerReconciler) reconcileServiceAccount(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	sa := gitlabutils.ServiceAccount(runnerctl.GitLabRunnerServiceAccount, cr.Namespace)
	lookupKey := types.NamespacedName{
		Name:      sa.Name,
		Namespace: cr.Namespace,
	}

	found := &corev1.ServiceAccount{}
	if err := r.Get(ctx, lookupKey, found); err != nil {
		if errors.IsNotFound(err) {
			if err := r.Create(ctx, sa); err != nil {
				return err
			}

			return nil
		}

		return err
	}

	return nil
}

func (r *RunnerReconciler) getRunnerRole(ctx context.Context, key types.NamespacedName, crNamespace string) (*rbacv1.Role, error) {
	role := &rbacv1.Role{}
	if err := r.Get(ctx, key, role); err != nil {
		return nil, err
	}

	role.ObjectMeta = metav1.ObjectMeta{
		Name:      key.Name,
		Namespace: crNamespace,
	}

	return role, nil
}

func (r *RunnerReconciler) getRunnerRoleBinding(ctx context.Context, key types.NamespacedName, crNamespace string) (*rbacv1.RoleBinding, error) {
	rb := &rbacv1.RoleBinding{}
	if err := r.Get(ctx, key, rb); err != nil {
		return nil, err
	}

	rb.ObjectMeta = metav1.ObjectMeta{
		Name:      key.Name,
		Namespace: crNamespace,
	}
	rb.Subjects[0].Namespace = rb.Namespace

	return rb, nil
}

// Check if the gitlab-runner-app-role exists.
// if not, get role from operator namespace and create
func (r *RunnerReconciler) reconcileRunnerRole(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	role := &rbacv1.Role{}
	roleKey := types.NamespacedName{Namespace: cr.Namespace, Name: runnerctl.GitLabRunnerRole}
	if err := r.Get(ctx, roleKey, role); err != nil {
		if errors.IsNotFound(err) {
			roleKey.Namespace = getOperatorNamespace()
			if role, err = r.getRunnerRole(ctx, roleKey, cr.Namespace); err != nil {
				return err
			}

			if err := r.Create(ctx, role); err != nil {
				if errors.IsAlreadyExists(err) {
					return nil
				}

				return err
			}
		}

		return err
	}

	return nil
}

// Check if the gitlab-runner-app-rolebinding exists.
// if not, get role from operator namespace and create
func (r *RunnerReconciler) reconcileRunnerRoleBinding(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	rb := &rbacv1.RoleBinding{}
	rbKey := types.NamespacedName{Namespace: cr.Namespace, Name: runnerctl.GitLabRunnerRoleBinding}
	if err := r.Get(ctx, rbKey, rb); err != nil {
		if errors.IsNotFound(err) {
			rbKey.Namespace = getOperatorNamespace()
			if rb, err = r.getRunnerRoleBinding(ctx, rbKey, cr.Namespace); err != nil {
				return err
			}

			if err := r.Create(ctx, rb); err != nil {
				if errors.IsAlreadyExists(err) {
					return nil
				}

				return err
			}
		}

		return err
	}

	return nil
}

func (r *RunnerReconciler) parseCustomEnvironments(ctx context.Context, cr *gitlabv1beta2.Runner) []corev1.EnvVar {
	environments := []corev1.EnvVar{}
	if cr.Spec.Environment == "" {
		return []corev1.EnvVar{}
	}

	envConfigMapKey := types.NamespacedName{
		Name:      cr.Spec.Environment,
		Namespace: cr.Namespace,
	}

	envConfigMap := &corev1.ConfigMap{}
	if err := r.Get(ctx, envConfigMapKey, envConfigMap); err != nil {
		return []corev1.EnvVar{}
	}

	for key, value := range envConfigMap.Data {
		environments = append(environments,
			corev1.EnvVar{
				Name:  key,
				Value: value,
			},
		)
	}

	return environments
}

func (r *RunnerReconciler) projectCleanup(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	runners := &gitlabv1beta2.RunnerList{}

	if err := r.List(ctx, runners, client.InNamespace(cr.Namespace)); err != nil {
		return err
	}

	operatorNS := getOperatorNamespace()
	if len(runners.Items) == 1 && cr.Namespace != operatorNS {
		// delete "gitlab-runner-sa" service account
		if err := r.cleanUpServiceAccount(ctx, cr.Namespace); err != nil {
			return err
		}

		// delete "gitlab-runner-app-role" role
		if err := r.cleanUpRole(ctx, cr.Namespace); err != nil {
			return err
		}

		// remove "gitlab-runner-app-rolebinding" role binding
		if err := r.cleanUpRoleBinding(ctx, cr.Namespace); err != nil {
			return err
		}
	}

	return nil
}

func (r *RunnerReconciler) cleanUpServiceAccount(ctx context.Context, namespace string) error {
	saKey := types.NamespacedName{
		Name:      runnerctl.GitLabRunnerServiceAccount,
		Namespace: namespace,
	}
	sa := gitlabutils.ServiceAccount(saKey.Name, saKey.Namespace)
	if err := r.Delete(ctx, sa); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}

func (r *RunnerReconciler) cleanUpRole(ctx context.Context, namespace string) error {
	roleKey := types.NamespacedName{
		Name:      runnerctl.GitLabRunnerRole,
		Namespace: namespace,
	}
	role := &rbacv1.Role{}
	if err := r.Get(ctx, roleKey, role); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	if err := r.Delete(ctx, role); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}

func (r *RunnerReconciler) cleanUpRoleBinding(ctx context.Context, namespace string) error {
	rbKey := types.NamespacedName{
		Name:      runnerctl.GitLabRunnerRoleBinding,
		Namespace: namespace,
	}
	rb := &rbacv1.RoleBinding{}
	if err := r.Get(ctx, rbKey, rb); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	if err := r.Delete(ctx, rb); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}
