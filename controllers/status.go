package controllers

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"reflect"
	"regexp"
	"time"

	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	// RunnerRunning indicates GitLab Runner
	// is Running
	RunnerRunning string = "Running"
	// RunnerPending indicates initiliazing
	RunnerPending string = "Pending"
	// RunnerFailed indicates error
	RunnerFailed string = "Failed"
	// RunnerWaiting indicates waiting for secret
	RunnerWaiting string = "Waiting"
)

func (r *RunnerReconciler) updateRunnerStatus(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	runner := &gitlabv1beta2.Runner{}
	runnerKey := types.NamespacedName{
		Name:      cr.Name,
		Namespace: cr.Namespace,
	}
	err := r.Get(ctx, runnerKey, runner)
	if err != nil {
		return err
	}

	if reflect.DeepEqual(runner.Status, cr.Status) {
		return nil
	}

	runner.Status = cr.Status
	return r.Status().Update(ctx, runner)
}

func (r *RunnerReconciler) reconcileStatus(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	runner := &gitlabv1beta2.Runner{}
	runnerKey := types.NamespacedName{
		Namespace: cr.Namespace,
		Name:      cr.Name,
	}
	if err := r.Get(ctx, runnerKey, runner); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}

		return err
	}

	podList, err := r.getDeploymentPods(ctx, cr)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}

		return err
	}

	if len(podList.Items) == 0 {
		runner.Status.Phase = RunnerPending
		return r.updateRunnerStatus(ctx, runner)
	}

	rand.Seed(time.Now().Unix())
	random := rand.Int() % len(podList.Items)
	targetPod := podList.Items[random]

	if targetPod.Status.Phase == corev1.PodPending {
		runner.Status.Phase = RunnerPending
		return r.updateRunnerStatus(ctx, runner)
	}

	var consoleLog string
	if targetPod.Status.Phase == corev1.PodRunning {
		if runner.Status.Phase != RunnerRunning {
			runner.Status.Phase = RunnerRunning

			// check console log
			consoleLog, err = getConsoleLog(&targetPod)
			if err != nil {
				return err
			}

			// if the registration status is not in console logs
			// return an error
			status, err := getRegistrationStatus(consoleLog)
			if err != nil {
				return err
			}

			runner.Status.Registration = status
			return r.updateRunnerStatus(ctx, runner)
		}

		return nil
	}

	if targetPod.Status.Phase == corev1.PodFailed {
		runner.Status.Phase = RunnerFailed
		return r.updateRunnerStatus(ctx, runner)
	}

	return nil
}

func (r *RunnerReconciler) getDeploymentPods(ctx context.Context, cr *gitlabv1beta2.Runner) (*corev1.PodList, error) {
	deployment := &appsv1.Deployment{}
	deployKey := types.NamespacedName{
		Namespace: cr.Namespace,
		Name:      fmt.Sprintf("%s-runner", cr.Name),
	}
	if err := r.Get(ctx, deployKey, deployment); err != nil {
		return nil, err
	}

	podLabels := deployment.Spec.Template.ObjectMeta.Labels
	options := &client.ListOptions{
		Namespace:     cr.Namespace,
		LabelSelector: labels.SelectorFromSet(podLabels),
	}

	podList := &corev1.PodList{}
	if err := r.List(ctx, podList, options); err != nil {
		return nil, err
	}

	return podList, nil
}

func getConsoleLog(pod *corev1.Pod) (string, error) {
	client, err := gitlabutils.KubernetesConfig().NewKubernetesClient()
	if err != nil {
		return "", err
	}

	logReader, err := client.CoreV1().RESTClient().Get().
		Resource("pods").SubResource("log").Namespace(pod.Namespace).Name(pod.Name).
		VersionedParams(&corev1.PodLogOptions{}, scheme.ParameterCodec).Stream(context.Background())
	if err != nil {
		return "", err
	}

	logs, err := ioutil.ReadAll(logReader)
	if err != nil {
		return "", err
	}

	return string(logs), nil
}

// Use regex to get the registration status from the console of
// the GitLab Runner pod.
// If not found, return an error.
func getRegistrationStatus(log string) (string, error) {
	re := regexp.MustCompile("(?m)Registering runner([.]+) ([a-z]+)")
	registration := re.FindSubmatch([]byte(log))

	if len(registration) == 0 {
		return "", fmt.Errorf("registration status unavailable")
	}

	return string(registration[len(registration)-1]), nil
}
